const Welcome =()=>{
    const fonction_1 =()=>{
        console.log("bonjour 1")
    }
    const fonction_2 =(text)=>{
        console.log(text)
    }

    return (
        <div>
            <button onClick={fonction_1}>test 1</button>
            <button onClick={() => fonction_2("bonjour 2")}>test 2</button>
            <button onClick={() => console.log("bonjour 3")}>test 3</button>


        </div>
    )
}
export default Welcome;