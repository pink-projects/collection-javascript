# Projet de Devinettes Numériques

Ce projet React est une application simple où les utilisateurs tentent de deviner un nombre. L'application est construite avec React et utilise des composants fonctionnels et de classe pour gérer l'état et les interactions.

## Fonctionnalités

- **But** : Le jeu commence avec un nombre aléatoire que l'utilisateur doit deviner. Ce nombre s'incrémente après chaque réponse correcte.
- **Feedback immédiat** : L'utilisateur reçoit un retour immédiat indiquant si sa supposition était trop haute, trop basse, ou correcte.
- **Interface intuitive** : L'interface utilisateur est simple et offre des instructions claires ainsi qu'un retour visuel pour guider l'utilisateur.

## Installation

Pour démarrer le projet localement, suivez ces étapes :

1. Clonez le dépôt :
   ```
   git clone https://gitlab.com/pink-projects/collection-React.git
   ```
2. Accédez au répertoire du projet :
   ```
   cd your-project-directory
   ```
3. Installez les dépendances nécessaires :
   ```
   npm install
   ```
4. Lancez l'application :
   ```
   npm start
   ```

## Usage

Ouvrez votre navigateur et accédez à `http://localhost:3000`. Le jeu s'affiche avec des instructions claires pour jouer.

### Instructions pour Jouer

1. **Commencer le jeu** : Lorsque vous ouvrez l'application, le jeu commence avec un nombre aléatoire généré que vous devez deviner.
2. **Faire une supposition** : Entrez votre supposition dans le champ prévu à cet effet et cliquez sur le bouton "Guess".
3. **Recevoir des indices** : Après chaque tentative, un indice vous indique si votre supposition était trop haute, trop basse, ou correcte.
4. **Continuer à jouer** : Si vous devinez correctement, le nombre est incrémenté et le jeu continue.

## Structure du Projet

- `App.js` : Configure l'application et inclut le composant `Parent`.
- `Parent.js` : Gère l'état global du jeu, y compris le nombre à deviner et les tentatives de l'utilisateur.
- `Child.js` : Affiche un champ de saisie où l'utilisateur peut entrer sa tentative et envoie cette valeur au composant `Parent`.

## Composants

### App

Le composant racine qui englobe toute l'application.

### Parent

- **Props**: Aucun
- **État**:
  - `answer`: Le nombre que l'utilisateur doit deviner.
  - `attempt`: La dernière tentative saisie par l'utilisateur.
- **Fonctionnalités**:
  - Permet à l'utilisateur de saisir des tentatives et de vérifier si la tentative est correcte.
  - Met à jour le nombre à deviner lorsque l'utilisateur trouve le bon nombre.

### Child

- **Props**:
  - `ans`: Le nombre actuel à deviner.
  - `onInputChange`: Fonction pour mettre à jour la tentative dans l'état du `Parent`.
- **Fonctionnalités**:
  - Affiche un champ de saisie pour l'utilisateur.

## Contribution

Les contributions sont les bienvenues. Veuillez forker le dépôt et proposer des pull requests, ou ouvrir des issues pour discuter des modifications potentielles ou signaler des bugs.
