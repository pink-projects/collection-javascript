import React from "react";

function Child({ans, onInputChange}){
    console.log(ans)
    return (
        <div>
            <h1 >Guess Box</h1>
            <input type="text" placeholder="Enter your guess here"onChange={e => onInputChange(e.target.value)}></input>
        </div>

    )
}
export default Child;