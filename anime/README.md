# Projet Liste d'Animes

Ce projet React est une application interactive qui permet aux utilisateurs de visualiser une liste d'animes avec des détails sur les personnages principaux et secondaires, ainsi que les visualisations. Les utilisateurs peuvent également augmenter le nombre de visualisations pour chaque anime.

## Fonctionnalités

- **Liste d'animes** : Affiche les noms des animes avec leurs personnages principaux et secondaires et le nombre de visualisations.
- **Interaction utilisateur** : Permet aux utilisateurs d'augmenter le nombre de visualisations de chaque anime.
- **Style dynamique** : Utilise des composants pour ajouter des styles et des interactions dynamiques, comme le changement de style au survol.

## Installation

Pour démarrer le projet localement, suivez ces étapes :

1. Clonez le dépôt :
   ```
   git clone https://gitlab.com/pink-projects/collection-React.git
   ```
2. Accédez au répertoire du projet :
   ```
   cd your-project-directory
   ```
3. Installez les dépendances nécessaires :
   ```
   npm install
   ```
4. Lancez l'application :
   ```
   npm start
   ```

## Usage

Ouvrez votre navigateur et accédez à `http://localhost:3000`. L'interface de l'application liste les animes disponibles avec des boutons pour augmenter les visualisations.

## Structure du Projet

- `App.js` : Le composant racine qui intègre tous les autres composants.
- `Wrapper.js` : Un composant de style qui enveloppe les éléments avec un style de boîte.
- `MyHeader.js` : Gère les en-têtes avec style dynamique au survol.
- `Welcome.js` : Contient des boutons pour tester différentes interactions.
- `Animes.js` : Présente les détails d'un anime spécifique.
- `AnimesListe.js` : Gère l'état global de la liste d'animes, permettant des interactions comme l'augmentation des visualisations.

## Composants

### App

Le composant principal qui lance l'application et intègre `AnimesListe`.

### AnimesListe

- **Props**:
  - `title`: Titre de la liste d'animes.
  - `color`: Couleur des titres.
- **État**:
  - `anime`: Liste des animes avec détails.
- **Fonctionnalités**:
  - Affiche une liste d'animes avec la possibilité d'augmenter les visualisations pour chaque anime.

### Animes

- **Props**:
  - `main`: Personnage principal de l'anime.
  - `second`: Personnage secondaire de l'anime.
  - `visualisation`: Nombre de visualisations de l'anime.
- **Fonctionnalités**:
  - Affiche les détails d'un anime spécifique.

### Wrapper

Enveloppe d'autres composants pour appliquer un style uniforme.

### MyHeader

- **Props**:
  - `color`: Couleur du texte de l'en-tête.
- **Fonctionnalités**:
  - Change de style au survol.

### Welcome

Contient des boutons pour démontrer les interactions simples.

## Contribution

Les contributions sont les bienvenues. Veuillez forker le dépôt et proposer des pull requests, ou ouvrir des issues pour discuter des modifications potentielles ou signaler des bugs.
