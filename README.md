# Collection d'Applications React

Ce dépôt contient deux applications React distinctes : une application de devinettes numériques et une application de liste d'animes. Chaque application offre des fonctionnalités interactives et est construite avec des composants fonctionnels et de classe pour gérer l'état et les interactions.

## Applications

### 1. Projet de Devinettes Numériques

- **But** : Les utilisateurs tentent de deviner un nombre aléatoire qui s'incrémente après chaque réponse correcte.
- **Fonctionnalités** :
  - Interface intuitive avec feedback immédiat.
  - Indications si la devinette est trop haute, trop basse, ou correcte.

### 2. Projet Liste d'Animes

- **But** : Affichage et gestion d'une liste d'animes avec la possibilité d'augmenter les visualisations pour chaque anime.
- **Fonctionnalités** :
  - Liste d'animes avec personnages principaux et secondaires.
  - Interaction pour augmenter le nombre de visualisations de chaque anime.
  - Styles dynamiques, notamment un changement de style au survol.

## Installation

Les instructions suivantes sont applicables à chaque projet :

1. Clonez le dépôt :
   ```
   git clone https://gitlab.com/pink-projects/collection-React.git
   ```
2. Accédez au répertoire du projet :
   ```
   cd your-project-directory
   ```

### Pour démarrer le Projet de Devinettes Numériques :

```bash
cd GuessingGame
npm install
npm start
```

### Pour démarrer le Projet Liste d'Animes :

```bash
cd AnimesList
npm install
npm start
```

## Usage

Ouvrez votre navigateur et accédez à `http://localhost:3000` pour interagir avec l'application sélectionnée.

## Structure du Projet

### Composants Communs

- `Wrapper.js` : Utilisé dans le projet Liste d'Animes pour appliquer un style uniforme aux composants.

### Projet de Devinettes Numériques

- `App.js`, `Parent.js`, `Child.js` : Gèrent le jeu de devinettes et l'interaction utilisateur.

### Projet Liste d'Animes

- `App.js`, `AnimesListe.js`, `Animes.js`, `MyHeader.js` : Gèrent l'affichage et les interactions de la liste d'animes.

## Contribution

Les contributions à chaque projet sont les bienvenues. Veuillez forker le dépôt et proposer des pull requests, ou ouvrir des issues pour discuter des modifications potentielles ou signaler des bugs.
