import React from "react";
import Wrapper from "./Wrapper";
function Animes({children,main,second,visualisation}){
    const mainC= main ?main:"vide";
    const secondC= second ?second:"vide";
    const visualisationC= visualisation ?visualisation:0;
    return children && (
     
            <Wrapper>
                <p>Anime: {children}</p>
                <p>Personnage principal: {mainC}</p>
                <p>Personnage secondaire :{secondC}</p>
                <p>Visualisation :{visualisationC}</p>
          
            </Wrapper>
        )
   
}

export default Animes;