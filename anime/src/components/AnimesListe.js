import React,{Component} from "react";
import Animes from "./Animes";
import Wrapper from "./Wrapper";
import MyHeader from "./MyHeader";


class AnimesListe extends Component{
    state= {
        anime:[
            {name:"Naruto", main:"Naruto",second:"Sasuke", visualisation: 0},
            {name:"Dragon Ball", main:"Goku",second:"Vegeta", visualisation: 10},
            {name:"One Piece", main:"Luffy", visualisation: 20},
            {name:"JJK", visualisation: 30},
        ]
      }
      increment=()=>{
        const update= this.state.anime.map((params)=> params.visualisation+=10000)
        this.setState({
            update
          })
      }
      
    render(){
        const {title}=this.props;
        const {col}=this.props;
        return (
       <div >
            <Wrapper>
                <MyHeader color={col}>{title}</MyHeader>
                <button onClick={this.increment}> +10000 views</button>

            </Wrapper>
            {
                this.state.anime.map(({name,main,second,visualisation},index)=>
                {
                    return <Animes key={index} main={main} second={second} visualisation={visualisation}>{name}</Animes>
                })
            }
       </div>
    )
    }
}
export default AnimesListe;