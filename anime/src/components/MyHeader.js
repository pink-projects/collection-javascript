
function MyHeader({children,color}){
    const addStyle=(e)=>{
        if(e.target.classList.contains('styled')){
            e.target.classList.remove('styled')
        }
        else e.target.classList.add('styled')
    
      }
    return(
        <div>
            <h1 onMouseOver={addStyle} style={{color:color}}>{children}</h1>
        </div>
    )
}

export default MyHeader;