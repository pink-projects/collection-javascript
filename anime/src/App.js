import logo from './logo.svg';
import './App.css';
import {Component} from 'react';
import AnimesListe from './components/AnimesListe';
import Wrapper from './components/Wrapper';
import Welcome from './components/Welcome';

class App extends Component {
  state= {
    title:"Catalogue d'anime",
    col:"pink"
  }

  changeTitle=(e)=>{
    
    this.setState({
      title:e.target.value
    })
  }
  render(){
    return (
      <div className="App">
        <Wrapper>
          <label >Changer le titre : </label>
          <input type='text' onChange={this.changeTitle} value={this.state.title}></input>
          <Welcome/>
        </Wrapper>
        <AnimesListe title={this.state.title} col={this.state.col}/>
      </div>
    );
  }
 
}

export default App;
