import React, {Component} from 'react'
import Child from './Child';
class Parent  extends Component{


    state={

        answer: 3,
        attempt: null,

    }
    handleInputChange = (value) => {
        this.setState({ attempt: value });
    }
    handleGuess = () => {
    if (parseInt(this.state.attempt) === this.state.answer) {
        alert("Correct!");
        this.setState({ answer: this.state.answer+=1 });
    } else {
        if(parseInt(this.state.attempt)>this.state.answer){
            alert("Too hight, try again!");
        }
        
        else {
            alert("Too low, try again!");
        }

    }
    }
      

    render(){
        return (
            <div>
                <h1>Number Guessing Game</h1>
                <p>Try to guess the number between 1 and 10. Enter your guess below and click "Guess" to see if you're right!</p>
                <button onClick={this.handleGuess}>Guess</button>
                <hr />
                <Child ans={this.state.answer} onInputChange={this.handleInputChange} />
            </div>
        )
    }
}
export default Parent;