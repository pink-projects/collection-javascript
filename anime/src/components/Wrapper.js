import Animes from "./Animes";
function Wrapper({children}){
    return    (<div style={{
        border: '2px solid #ccc',
        padding: '10px',         
        margin: '10px',            
        borderRadius: '8px',   
        boxShadow: '0 2px 5px rgba(0,0,0,0.1)' 
    }}>{children}

    </div>)
}
export default Wrapper;